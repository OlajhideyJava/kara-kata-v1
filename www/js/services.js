/**
 * Created by hp on 3/3/2017.
 */

var inventoryService = angular.module('inventory.service', ['backand']);

inventoryService.service('StockService', function ($http, Backand) {

    var baseUrl = '/1/objects/';

  //object name of the user
    var objectName = 'user/';

  //object name of the uploaded stock
    var stockUrl = 'stock/';

  //object name of the sales made
    var salesUrl = 'sales';


  //Full url of the User object
  function getUrl() {
    return Backand.getApiUrl() + baseUrl+objectName;
  };

  //Url for the stock object
  function getUploadStockUrl () {
    return Backand.getApiUrl() + baseUrl + stockUrl;
  }

  //Url for the sales object
  function getSalesUrl () {
    return Backand.getApiUrl() + baseUrl + salesUrl
  }

    //Call to get object of a particular stock
  function getUrlforStckId (id) {
    return getUploadStockUrl() + id;
  }

  //Call to get to user in an object
  function getUrlForId(id) {
    return getUrl + id;
  }

  function getUrlQuerySales(){
    return Backand.getApiUrl() + queryUrl + saleQueName
  }

    //function to perfrom register
    doRegister = function(info) {
      return $http.post(getUrl(), info)
    };

    //function to perform login
    doLogin = function(loginData) {
      return $http.get(getUrl(), loginData)
    }

  //function to upload stock
    uploadStock = function(stocks) {
      return $http.post(getUploadStockUrl(),stocks );
    }

  //function to make sales
    makeSales = function (sales) {
      return $http.post(getSalesUrl(), sales)
    }



  querySales = function(id, quantity, stock_quantity) {
    return $http({
      method: 'PUT',
      url: Backand.getApiUrl() + baseUrl + stockUrl + '/' + id,
      data: {
        "quantity" :  stock_quantity- quantity
      }
    })
  }

  updateStock = function(id, new_quantity,old_quantity){
    return $http({
      method: 'PUT',
      url: Backand.getApiUrl() + baseUrl + stockUrl + '/'+id,
      data: {
        "quantity" : old_quantity+new_quantity
      }
    })
  }

  deleteStock = function(id) {
    return  $http.delete(getUrlforStckId(id))
  }

  //function to get all stocks
    fetchStock = function(userid) {
      return $http ({
        method: 'GET',
        url: getUploadStockUrl(),
        params: {
          pageSize: 1000,
          filter: [
            {
              fieldName: 'userId',
              operator: 'in',
              value: userid
            }
          ],
          sort: [ {
            "fieldName" : "name",
            "order" : "asc"
          }]
        }
      });
    }

  //function to get user profile
  userProfile = function(id){
    return $http({
      method: 'GET',
      url: Backand.getApiUrl() + baseUrl + objectName,
      params: {
        filter: [ {
          fieldName : "id",
          operator : "equals",
          value : id
          }
      ]
          }
      }
  )
}

  

  salesActivity = function(userId){
    return $http({
      method : 'GET',
      url: Backand.getApiUrl() + baseUrl + salesUrl,
      params: {
        filter: [
            {
              "fieldName": "userid",
              "operator": "equals",
              "value": userId
            }
          ],

      }
      
      //     sort: [{ 
      //       "fieldName": "id",
      //       "order" : "desc"
      //     }]

      })
  }

  //this enable the functions so as to be visible in the controllers
  return {
    doRegister : doRegister,
    doLogin : doLogin,
    uploadStock : uploadStock,
    fetchStock : fetchStock,
    makeSales : makeSales,
    userProfile: userProfile,
    querySales: querySales,
    salesActivity: salesActivity,
    updateStock : updateStock,
    deleteStock : deleteStock
  }
})
