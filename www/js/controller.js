var inventCtrl = angular.module('inventory.controller',['backand']);

var userId = undefined;
var allStocks = [];
var cart = [];
var userData = {};
var total = 0;
inventCtrl.controller('LoginController', function($scope, $state,StockService, $http, $ionicPopup, $ionicLoading,$ionicHistory){

  //to show the loading spinner
    $scope.show = function () {
      $ionicLoading.show({
        template: '<p>Loading...</p><ion-spinner></ion-spinner>'
      });
    }

  //function to hide the loading spinner
  $scope.hide = function () {
    $ionicLoading.hide();
  }

  //an object that takes the login values from the front end
	$scope.loginData = {};

  $ionicHistory.clearHistory();

  $ionicHistory.nextViewOptions({
    disableAnimate: true,
    disableBack: true
  })

  //function when button clicked in the front end
	$scope.doLogin = function() {

    //display the loading spinner
    $scope.show($ionicLoading);

    //fetch login function from service
    StockService.doLogin($scope.loginData).then(function(result){

      //Check if the user exists in the database
      for(var i = 0 ; i <= result.data.data.length; i++){

        //values needed from the result of the request
        
        $scope.username = result.data.data[i].phone;
        $scope.password = result.data.data[i].password;

        //conditions for the login
        if($scope.loginData.phone == $scope.username && $scope.loginData.password == $scope.password) {
           userId = result.data.data[i].id;
          //function to fetch stock when app is loaded
          function fetchStock(userId) {
            StockService.fetchStock(userId).then(function(result){
              //assign data to an empty array
              allStocks = result.data.data;
            }, function(error) {
               $ionicPopup.alert({
                 title: 'Error',
                 template: 'Couldnt connect to the server Please check internet Connection'
               })
            })
          };

          //function to fetch user profile
          StockService.userProfile(userId).then(function (result) {
           userData = result.data.data[0];
           console.log(userData);
          }, function (error) {
            $ionicPopup.alert({
                 title: 'Error',
                 template: 'Couldnt connect to the server Please check internet Connection'
               })
          })


          fetchStock(userId);

          $state.go('tabs.dashboard')
          //else statement if the user data cant be found in the database
       }else if($scope.loginData.phone == $scope.username || $scope.loginData.password == $scope.password){
          $ionicPopup.alert({
            title: 'Error',
            template: 'Incorrect username or password'
          })
       }

     }

   }, function(error) {
    if(error.status == -1) {
      $ionicPopup.alert({
         title: 'Error',
         template: 'Couldnt connect to the server Please check internet Connection'
      })
    }
   }).finally(function(){
   $scope.hide($ionicLoading);
     if(error.status == -1) {
      $ionicPopup.alert({
         title: 'Error',
         template: 'Couldnt connect to the server Please check internet Connection'
      })
    }
    });

}

})

inventCtrl.controller('RegisterController', function($scope, Backand, StockService, $ionicPopup, $state, $ionicLoading){

  //to show the loading spinner
  $scope.show = function () {
    $ionicLoading.show({
      template: '<p>Loading<ion-spinner></ion-spinner></p>'
    })
  }

  //function to hide the loading spinner
  $scope.hide = function(){
    $ionicLoading.hide();
  }

  //an object that takes the register values from the front end
  $scope.details = {};

  $scope.signup = function() {
      StockService.doRegister($scope.details).then(function (result) {

        $scope.show($ionicLoading);

        $ionicPopup.alert({
          title : 'Success',
          template : 'Account Created successfully'
        })
        $state.go('login');
      }, function(error){

          if(error.status == -1) {
            $ionicPopup.alert({
                 title: 'Error',
                 template: 'Couldnt connect to the server Please check internet Connection and try again'
               })
          }
      }).finally(function () {
        $scope.hide($ionicLoading);
      })
  }

})

inventCtrl.controller('DashboardController', function($scope, StockService, $state, $ionicHistory,$ionicPopup, $ionicPopover, Backand){

  $ionicHistory.clearHistory();

  $ionicPopover.fromTemplateUrl('templates/my-popover.html', {
    scope: $scope
  }).then(function(popover) {
    $scope.popover = popover;
  });

  $scope.openPopover = function($event) {
    $scope.popover.show($event);
  };

  $scope.closePopover = function() {
    $scope.popover.hide();
  };

  //Cleanup the popover when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.popover.remove();
  });

  // Execute action on hidden popover
  $scope.$on('popover.hidden', function() {
    // Execute action

  });
  // Execute action on remove popover
  $scope.$on('popover.removed', function() {
    // Execute action
  });

	$scope.next = function(){
		$state.go('tabs.upload');
	}

	$scope.make_sales = function(){
   $scope.fetchStock =  function(userId) {
      StockService.fetchStock(userId).then(function(result){
        allStocks = result.data.data;

      }, function(error) {
        $ionicPopup.alert({
          title: 'Error',
          template: 'Couldnt connect to the server Please check internet Connection'
        })
      })
    };

    $scope.fetchStock(userId);
		$state.go('sales');
	}

	$scope.stock_manag = function(){
		$state.go('stock_management')
	}

  $scope.updatePage = function() {
    $state.go('update_stock')
  }

  $scope.deletePage = function() {
    $state.go('delete_stock')
  }
})

inventCtrl.controller('ProfileController', function($scope, $state, StockService, $ionicModal, $ionicHistory){
  $scope.userData = userData;
  $scope.stock_activity = [];
  $scope.stock_details = {};
  $scope.userId = userId
  $scope.logout = function(){
      $ionicHistory.clearHistory();
      userId = undefined
      allStock = [];
		  $state.go('login')

    $ionicHistory.nextViewOptions({
    disableAnimate: true,
    disableBack: true
  })
	};

 $scope.loadProfile = function(){
    StockService.userProfile($scope.userId).then(function (result) {
      $scope.userData = result.data.data[0]
    }, function (error) {
      console.log(error);
    })
  }

  $scope.fullname = $scope.userData.fullname;
  $scope.email = $scope.userData.email;
  $scope.phone = $scope.userData.phone
  $scope.storename = $scope.userData.storename

 

  $scope.loadProfile();

 $scope.stock_length = $scope.stock_activity.length;

  $scope.refreshActivity = function(userId){
    StockService.salesActivity($scope.userId).then(function(result) {

      $scope.stock_activity = result.data.data;
      $scope.stock_length = $scope.stock_activity.length;
  
    }, function(error) {
      console.log(error);
    }).finally(function() {
       $scope.$broadcast('scroll.refreshComplete')
    })
  }

  $scope.refreshActivity(userId)
 $ionicModal.fromTemplateUrl('templates/stock_details.html',
    {
      scope : $scope,
      animation : 'slide-in-up'
    }).then(function (modal) {
      $scope.modal = modal;
  })

  $scope.showModal = function (details) {
    $scope.stock_details = details
    console.log($scope.stock_details);
    $scope.modal.show();
  }

  $scope.hideModal = function() {
    $scope.modal.hide();
  }

  $scope.$on('$destroy', function(){
    //Cleans up the modal whenever we are done with it
    $scope.modal.remove();
  });

  // Execute action on hide modal
  $scope.$on('modal.hidden', function() {
    // Execute action

  });
  // Execute action on remove modal
  $scope.$on('modal.removed', function() {
    // Execute action
  });

  // $scope.refreshActivity();


})

inventCtrl.controller('UploadController', function($scope, $state, $ionicModal, $ionicPopup,$ionicHistory,$ionicLoading ,StockService){
      $scope.show = false;
      $scope.stock = undefined;
      $scope.repeat = [];
      $scope.cart = [];
      $scope.my_stocks = {};

    $scope.showLoad = function () {
      $ionicLoading.show({
        template: '<p>Loading...</p><ion-spinner></ion-spinner>'
      });
    }

    $scope.hide = function () {
      $ionicLoading.hide();
    }

  $ionicModal.fromTemplateUrl('templates/finalUpload.html',
    {
      scope : $scope,
      animation : 'slide-in-up'
    }).then(function (modal) {
      $scope.modal = modal;
  })

  $scope.showModal = function () {
    $scope.modal.show();
  }

  $scope.hideModal = function() {
    $scope.modal.hide();
  }

  $scope.$on('$destroy', function(){
    //Cleans up the modal whenever we are done with it
    $scope.modal.remove();
  });

  // Execute action on hide modal
  $scope.$on('modal.hidden', function() {
    // Execute action

  });
  // Execute action on remove modal
  $scope.$on('modal.removed', function() {
    // Execute action
  });
  $scope.tryUpload = function(stock) {
    $scope.show = true;
    $scope.stock = stock;
    for(i=0;i<$scope.stock;i++){
      $scope.repeat.push(i);
    }
    console.log($scope.repeat);

    $scope.add2cart = function (i,prd_name, qty) {
      // if((prd_name == undefined && qty == undefined) || (prd_name == undefined && qty == undefined)) {
      //   $ionicPopup.alert({
      //     title : 'Oops',
      //     template: 'Product name and Quantity cannot be left empty'
      //   })
      // }else{
        $scope.cart.push({
          prd_name : prd_name,
          qty : qty
        })
        $scope.cartLength = $scope.cart.length;
      // }

    };
    $scope.cartLength = $scope.cart.length
  };

  $scope.remove = function(i) {
    $scope.cart.splice(i, 1);
  }

  $scope.uploadStock = function() {
    $scope.showLoad($ionicLoading);
    for(var i=0; i<$scope.cart.length; i++) {
      $scope.name = $scope.cart[i].prd_name;
      $scope.quantity = $scope.cart[i].qty;
      $scope.userId = userId;


      StockService.uploadStock(data = {"name": $scope.name, "quantity": $scope.quantity, "userId": $scope.userId}).then(function(result) {
        console.log(result);

      }, function (error) {
        console.log(error)
      }).finally(function () {
         $scope.hide($ionicLoading);
        $ionicPopup.alert({
          title : 'Success',
          template: 'Stock uploaded successfully'
        })
      })
    }
  }
  $scope.back = function(){
		$ionicHistory.goBack();
	}

})

inventCtrl.controller('SalesController', function($scope, $state, $ionicPopup,$rootScope, StockService, $ionicLoading,$ionicHistory, Backand){
    
  $scope.sale_id = Math.floor(Math.random() * 1000) + 1000;
  fetchStock(userId);
	$scope.display = false;
	$rootScope.cart = [];
	$scope.is_cart_empty = false;
	$scope.num_of_product = 0;
  $scope.allStock = [];
  $scope.allStock = allStocks;
  $scope.status = 1;

  $scope.fetchStock = function() {
    StockService.fetchStock(userId).then(function(result){
      $scope.allStock = result.data.data;
    }, function(error) {
      console.log(error);
  })
  }
  
  $scope.fetchStock();

  $scope.doRefresh = function () {
    StockService.fetchStock(userId).then(function(result){
      $scope.allStock = result.data.data;
    }, function(error) {
      console.log(error);
  }).finally(function () {
      // Stop the ion-refresher from spinning
      $scope.$broadcast('scroll.refreshComplete')

  })
  }
  $scope.all_length = $scope.allStock.length
  $scope.show = function () {
    $ionicLoading.show(
      {
        template: "<p>Loading</p><ion-spinner></ion-spinner>"
      }
    );
  }

  $scope.hide = function () {
    $ionicLoading.hide();
  }
	$scope.addToCart = function(id,name,quantity, stock_quantity){
		cart.push({id: id ,name: name, quantity: quantity, stock_quantity : stock_quantity})
		$scope.is_cart_empty = true;
		$scope.num_of_product = $scope.cart.length;
  
	}

	$scope.doSomething = function(){
		$scope.display = true;
	}

	$scope.back = function(){
		$state.go('tabs.dashboard');
     $ionicHistory.clearCache().then(function(result) {
        })

	}

	$scope.back_to_sales= function(){
		$state.go('sales')
	}

  $rootScope.cart = cart
  
  // console.log($scope.cart)

	$scope.final_sales = function(){

		$state.go('final_sales');
	}

	$scope.remove = function(x){
    $rootScope.cart.splice(x,1);
	}

  
	$scope.done = function(){

    $scope.show($ionicLoading);
    for(var i=0; i < $scope.cart.length; i++) {
      $scope.userId = userId;
      $scope.id = $scope.cart[i].id;
      $scope.name =  $scope.cart[i].name;
      $scope.quantity = $scope.cart[i].quantity;
      $scope.stock_quantity = $scope.cart[i].stock_quantity;

      StockService.querySales($scope.id, $scope.quantity, $scope.stock_quantity).then(function(result) {
       
      }, function(error) {
        if(error.status == -1) {
          $ionicPopup.alert({
            template: 'Couldnt Make sales please check internet connection'
          })
        }
      })


      var date = new Date();
      StockService.makeSales(data = {"userid": $scope.userId, "name" : $scope.name, "quantity" :  $scope.quantity, "sales_date": date, "status" : $scope.status}).then(function (result) {
            
        // Backand.on("items_updated", function(data) {
        //   console.log("i am here")
        // })

      $ionicPopup.alert({
          title: 'Success',
          template: '<div style="color: green; text-align: center"><i class="icon ion-checkmark"></i>	<span>Sales made successfully</span> </div>'
        }).then(function () {
          $state.go('tabs.dashboard');
          $scope.num_of_product = 0;
            
          $ionicHistory.clearCache().then(function(result) {
        })
         
        })
      }, function (error) {
        alert("error");
      }).finally(function () {
           $scope.hide($ionicLoading);
           cart = [];
           $rootScope.cart = [];
           $ionicHistory.clearCache().then(function(result) {
        })

      $scope.doRefresh();
      })
    }
	}

  $scope.stock_length = $scope.allStock.length;

	$scope.closeSearch= function(){
		$scope.display = false;
	}
});

inventCtrl.controller('StockController', function($scope, $state, StockService, $ionicHistory){
  $scope.display = false;
  $scope.allStock = [];
  $scope.stock_activity = [];
  $scope.total = 0;
  $scope.sales_length = 0;
  $scope.openSearchbar = function() {
    $scope.display = true;
  }

  $scope.fetchStock = function(userId) {
    StockService.fetchStock(userId).then(function(result) {
      $scope.allStock = result.data.data;

      for(var i =0; i< $scope.allStock.length; i++) {
           $scope.total += Number($scope.allStock[i].quantity);
        }
       })

     StockService.salesActivity(userId).then(function(result) {
      $scope.stock_activity = result.data.data;
      $scope.sales_length = $scope.stock_activity.length;
      console.log($scope.stock_activity);
    }, function(error) {
      console.log(error);
    })
  }

  $scope.fetchStock(userId)




  $scope.closeSearch = function() {
    $scope.display = false;
  }

  $scope.doRefresh = function() {
      StockService.salesActivity(userId).then(function(result) {
      $scope.stock_activity = result.data.data;
      $scope.sales_length = $scope.stock_activity.length;
      console.log($scope.stock_activity);
    })
    StockService.fetchStock(userId).then(function(result) {
      $scope.allStock = result.data.data;
      

    }).finally(function() {
       $scope.$broadcast('scroll.refreshComplete')
    })
  }

	$scope.back_dash = function(){
		$ionicHistory.goBack();
	}

})

inventCtrl.controller('UpdateController', function($scope,StockService, $state, $ionicPopup, $ionicLoading) {

  $scope.stocks = [];
  $scope.status = 0
  $scope.userId = userId
  var date = new Date();
     $scope.show = function () {
      $ionicLoading.show({
        template: '<p>Loading...</p><ion-spinner></ion-spinner>'
      });
    }

    $scope.hideLoading = function($ionicLoading) {
      $ionicLoading.hide();
    }

    $scope.fetchStock = function() {
       StockService.fetchStock(userId).then(function(result){
              //assign data to an empty array
              $scope.stocks = result.data.data;
              console.log($scope.stocks)
            }, function(error) {
              console.log(error);
            })
       }
      $scope.fetchStock();

    $scope.dashPage = function() {
      $state.go('tabs.dashboard');
    }

    $scope.doUpdate = function(id, quantity, new_quantity, name) {
      var confirmPopup = $ionicPopup.confirm({
        title: 'Warning',
        template: 'Are you sure you want to update this stock?'
      });

      confirmPopup.then(function(res) {
        if(res) {
          $scope.show($ionicLoading)
          StockService.updateStock(id, quantity, new_quantity).then(function(result){
            $ionicPopup.alert({
              title : 'Success',
              template: 'Stock updated successfully'
            })

            StockService.makeSales(data = {"userid": $scope.userId, "name" : name, "quantity" :  quantity, "sales_date": date, "status" : $scope.status})
            .then(function(result) {
              console.log(result)
            })

          }, function(error) {
             if(error.status == -1) {
                $ionicPopup.alert({
                  title: 'Couldnt update stock',
                  template: 'Something went wrong we couldnt update the stock'
                })
              }
          }).finally(function() {
            $scope.hideLoading($ionicLoading);
          })
        }else {
          console.log("i am not sure")
        }
      })
    }

    $scope.doRefresh = function() {
       StockService.fetchStock(userId).then(function(result){
              //assign data to an empty array
              $scope.stocks = result.data.data;
            }, function(error) {

            }).finally(function() {
              $scope.$broadcast('scroll.refreshComplete');;
            })
    }
})

inventCtrl.controller('DeleteController', function($scope, $state,StockService, $ionicLoading, $ionicPopup ) {
  $scope.stocks = [];
  $scope.status = -1;
  $scope.userId = userId
  var date = new Date()
    $scope.dashPage = function() {
      $state.go('tabs.dashboard');
    }

  

    $scope.hide = function () {
      $ionicLoading.hide();
    }

     $scope.fetchStock = function() {
       StockService.fetchStock(userId).then(function(result){
              //assign data to an empty array
              $scope.stocks = result.data.data;
            }, function(error) {
              console.log(error);
            })
       }

       $scope.deleteStock = function(i,id, name, quantity) {

       var confirmPopup = $ionicPopup.confirm({
              template: 'Are you sure you want to update this stock?'
        });

       confirmPopup.then(function(res) {

        

        if(res) {
         
          $scope.stocks.splice(i,1)


         StockService.deleteStock(id).then(function(result){
            StockService.makeSales(data = {"userid": $scope.userId, "name" : name, "quantity" :  quantity, "sales_date": date, "status" : $scope.status})
            .then(function(result) {
              console.log(result)
            })



           }, function(error) {
            $ionicPopup.alert({
              template: 'Unable to delete stock try again later'
            })
          })
       }else{
          console.log("i am not sure")
       }
    })

     }

          $scope.doRefresh = function() {
       StockService.fetchStock(userId).then(function(result){
              //assign data to an empty array
              $scope.stocks = result.data.data;
            }, function(error) {

            }).finally(function() {
              $scope.$broadcast('scroll.refreshComplete');;
            })
    }

$scope.fetchStock();

})
